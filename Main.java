import java.text.Normalizer;
import java.util.Objects;
import java.util.Scanner;

/**
 * This class demonstrates a simple program to find the position of a character in a given string.
 */
public class Main {

    /**
     * The main entry point for the program.
     *
     * @param args The command-line arguments (not used in this program).
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter your phrase: ");
        String phrase = scanner.nextLine();

        System.out.print("Enter the character to search for: ");
        String caractere = scanner.nextLine();

        int index = findPosition(phrase, caractere);

        if (index != -1) {
            System.out.println("On retrouve le caractère  "+ caractere +"  à la position  "+ index);
        }
        else{
            System.out.println("Le caractère "+ caractere +" n'est pas présent dans la phrase.");
        }
    }

    /**
     * Finds the position of a caractere in a given string.
     *
     * @param text The input string to search within.
     * @param caractere The caractere to search for.
     * @return The position of the caractere in the string, or -1 if not found.
     */
    public static int findPosition(String text, String caractere) {
        for (int i = 0; i < text.length(); i++) {
            String letter = Character.toString(text.charAt(i));
            if (Objects.equals(letter, caractere)) {
                return i;
            }
        }
        return -1;
    }
}
